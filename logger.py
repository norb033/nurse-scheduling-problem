import logging

class Configured_Logger:
    def __init__(self):
        file_formatter = logging.Formatter("%(asctime)s %(levelname)s %(module)s.%(funcName)s(%(lineno)d)\t %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p")
        stream_formatter = logging.Formatter("%(name)s: %(levelname)s %(funcName)s(%(lineno)d) %(message)s")

        #File to log to
        log_file = "./logfile.log"

        #Setup File handler
        self.file_handler = logging.FileHandler(log_file)
        self.file_handler.setFormatter(file_formatter)
        self.file_handler.setLevel(logging.DEBUG)

        #Setup Stream Handler (i.e. console)
        self.stream_handler = logging.StreamHandler()
        self.stream_handler.setFormatter(stream_formatter)
        self.stream_handler.setLevel(logging.INFO)

    def get_logger(self):
        #Get the logger
        app_log = logging.getLogger("app_logger")
        app_log.setLevel(logging.DEBUG)

        #Add both Handlers
        if len(app_log.handlers) > 0:
            return app_log
        app_log.addHandler(self.file_handler)
        app_log.addHandler(self.stream_handler)
        return app_log

LOG = Configured_Logger().get_logger()
