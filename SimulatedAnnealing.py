"""
    NSP - Nurse Scheduling Problem
    A solution for NSP with simulated annealing.
"""

import random as rnd
import copy as cp
from typing import Callable
from time import perf_counter
from numpy import exp, log

from Optimization import Optimization
from Nurse import Nurse
from constants import NUMBER_OF_NURSES, NUMBER_OF_DAYS, MAX_ITERATION_SA
from logger import LOG

def _anneal(iteration_count: int, initial_temperature: float, func: Callable) -> float:
    return func(iteration_count, initial_temperature)

def _exponential_annealing(iteration_count: int, initial_temperature: float) -> float:
    alpha = 0.85
    return initial_temperature * alpha**iteration_count

def _logaritmic_annealing(iteration_count: int, initial_temperature: float) -> float:
    alpha = 1.2
    return initial_temperature / (1 + alpha * log(1 + iteration_count))

def _linear_annealing(iteration_count: int, initial_temperature: float) -> float:
    alpha = 1
    return initial_temperature / (1 + alpha * iteration_count)

def _qvadratic_annealing(iteration_count: int, initial_temperature: float) -> float:
    alpha = 1
    return initial_temperature / (1 + alpha * iteration_count**2)

class SimulatedAnnealing(Optimization):
    annealing_functions = {'1': _exponential_annealing, 'exp': _exponential_annealing,
                      '2': _logaritmic_annealing, 'log': _logaritmic_annealing,
                      '3': _linear_annealing, 'lin': _linear_annealing,
                      '4': _qvadratic_annealing, 'qvad': _qvadratic_annealing}
    def __init__(self, initial_schedule: list[Nurse], minimum_staff_number: list[tuple]) -> None:
        super().__init__(initial_schedule, minimum_staff_number)
        self.schedule = cp.deepcopy(initial_schedule)
    def mutate(self, schedule: list[Nurse]) -> list[Nurse]:
        new_schedule = cp.deepcopy(schedule)
        # swap 2*n random nurses' 2 random days
        for _ in range(rnd.randint(1, 10)):
            nurse1, nurse2 = tuple(rnd.sample(list(range(NUMBER_OF_NURSES)), 2))
            day1, day2 = rnd.randint(0, NUMBER_OF_DAYS - 1), rnd.randint(0, NUMBER_OF_DAYS - 1)
            new_schedule[nurse1].schedule[day1], new_schedule[nurse2].schedule[day2] = new_schedule[nurse2].schedule[day2], new_schedule[nurse1].schedule[day1]
        return new_schedule
    def simulate(self, t0: float, annealing_type) -> list[Nurse]:
        anneal_function = self.annealing_functions[annealing_type]
        best_schedule = cp.deepcopy(self.initial_schedule)
        temperature = cp.copy(t0)
        iteration_count = 0

        while (temperature > 0.0001 and iteration_count < MAX_ITERATION_SA):
            new_schedule = self.mutate(self.schedule)

            current_schedule_value = self.evaluate_schedule(self.schedule)
            new_schedule_value = self.evaluate_schedule(new_schedule)

            if new_schedule_value < current_schedule_value or rnd.random() < exp((current_schedule_value - new_schedule_value) / temperature):
                self.schedule = cp.deepcopy(new_schedule)

            current_schedule_value = self.evaluate_schedule(self.schedule)
            best_schedule_value = self.evaluate_schedule(best_schedule)

            if current_schedule_value < best_schedule_value:
                best_schedule = cp.deepcopy(self.schedule)

            temperature = _anneal(iteration_count, t0, anneal_function)
            iteration_count += 1

        # LOG.info("\nReached iteration:\t%d", iteration_count)
        # LOG.info("Reached temperature:\t%.6f", temperature)

        return best_schedule
    def experiment(self, results: list[int], times: list[float], annealing_type: str, temperature: int) -> None:
        average_penalty_point = sum(results) / len(results)
        average_run_time = sum(times) / len(times)
        LOG.info("Average penalty for best schedule with %s and base temperature of %d degrees: %d", annealing_type, temperature, average_penalty_point)
        LOG.info("Average running time: %.4f s", average_run_time)
        LOG.info("Average improvement: %.2f%s", (1 - average_penalty_point / self.evaluate_schedule(self.initial_schedule)) * 100, '%')
    def test(self) -> None:
        LOG.info("Patameters: number of days = %d and number of nurses = %d", NUMBER_OF_DAYS, NUMBER_OF_NURSES)
        experiment_count = 30
        temperature = 1000
        anneling_type = "qvad"
        results = []
        times = []
        for _ in range(experiment_count):
            start_time = perf_counter()
            best_schedule = self.simulate(temperature, anneling_type)
            end_time = perf_counter()
            results.append(self.evaluate_schedule(best_schedule))
            times.append(end_time - start_time)
        self.experiment(results, times, self.annealing_functions[anneling_type].__name__, temperature)
        self.display(best_schedule,
                    end_time - start_time,
                    f"best schedule(last) with {self.__class__.__name__}")
