"""
    NSP - Nurse Scheduling Problem
    There are three shifts a day:
        - morning shift
        - afternoon shift
        - night shift
        /* + day off */
    In this version of NSP I am going to use Soft constraints as well as Hard constraints too.
    I assume that there are no holidays and the schedules are repeatable, so every day will be treated the same
    Soft constraints:
        - Each nurse may work no more than 7 consecutive days / days in a row
    Hard constraints:
        - Each schedule must satisfy the hospital's minimum coverage constraint(for)
        - No nurse may work a night shift followed by a morning shift
        - No nurse can work more than 1 shift a day
"""

import random as rnd

from utils import initalise_schedule
from SimulatedAnnealing import SimulatedAnnealing
from GeneticAlgorithm import GeneticAlgorithm
from constants import NUMBER_OF_DAYS

def main():
    minimum_staff_number = [(rnd.randint(1, 3), rnd.randint(1, 3), rnd.randint(1, 3), 0) for _ in range(NUMBER_OF_DAYS)]

    initial_schedule = initalise_schedule()
    SA = SimulatedAnnealing(initial_schedule, minimum_staff_number)
    EA = GeneticAlgorithm(initial_schedule, minimum_staff_number)

    SA.display(initial_schedule, 0, "initial schedule")
    SA.test()
    EA.test()

if __name__ == "__main__":
    main()
