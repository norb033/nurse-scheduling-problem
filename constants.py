"""This module is collecting all the global constants"""

class Day:
    """ The Day class defines the days of the week. The days are wrapped into a class,
    because if an object of the class is defined as a constant it is easier to be used later on.
    """
    def __init__(self):
        self.monday = 0
        self.tuesday = 1
        self.wednesday = 2
        self.thursday = 3
        self.friday = 4
        self.saturday = 5
        self.sunday = 6

DAYS = Day()

class Shift:
    """ The Shift class defines the possible shifts.
    It is much comfortable to use classes instead of dicts for global constants.
    """
    def __init__(self):
        self.morning = 0
        self.afternoon = 1
        self.night = 2
        self.day_off = 3

SHIFTS = Shift()


class Penalty:
    """ The Penalty class defines all the possible penalties.
    It is much comfortable to use classes instead of dicts for global constants.

    """
    def __init__(self):
        """Hospital's minimum shift coverage not satisfied."""
        self.not_enough_nurse_on_shift = 75
        """A nurse works more than 1 shift a day."""
        self.too_many_shifts_a_day = 15
        """A nurse works a night shift followed by a morning shift."""
        self.too_short_break_between_shifts = 30
        """A nurse works more than 7 consecutive days."""
        self.too_many_work_days = 10

PENALTY = Penalty()

NUMBER_OF_NURSES = 10
NUMBER_OF_DAYS = 3
MAX_ITERATION_SA = 10000
MAX_ITERATION_EA = 100

POPULATION_SIZE = 15
NUMBER_OF_NEW_ENTITIES = 10
CROSSOVER_RATE = 0.85
MUTATION_RATE = 0.7

DISPLAY = False
DISPLAY_SCHEDULE_IN_CONSOLE = True
WRITE_TO_OUT_FILE = True
RESULTS_FILE = "result.out"
