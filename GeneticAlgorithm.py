"""
    NSP - Nurse Scheduling Problem
    A solution for NSP with genetic algorithm.
"""

import random as rnd
import copy as cp
from time import perf_counter

from Nurse import Nurse
from Optimization import Optimization
from constants import CROSSOVER_RATE, MUTATION_RATE, NUMBER_OF_NEW_ENTITIES, NUMBER_OF_NURSES, NUMBER_OF_DAYS, MAX_ITERATION_EA, POPULATION_SIZE
from logger import LOG

class GeneticAlgorithm(Optimization):
    def __init__(self, initial_schedule: list[Nurse], minimum_staff_number: list[tuple]) -> None:
        super().__init__(initial_schedule, minimum_staff_number)
        self.initial_population = [self.mutate(initial_schedule) for _ in range(POPULATION_SIZE)]
        self.population = cp.deepcopy(self.initial_population)
    def cross_over(self) -> list[list[Nurse]]:
        children = []
        length = len(self.population)
        for i in range(length):
            for j in range(i + 1, length):
                if rnd.random() < CROSSOVER_RATE:
                    first_parent, second_parent = self.population[i], self.population[j]
                    combined = [((first_parent[ind], second_parent[ind]) if rnd.random() < 1/2 else ((second_parent[ind], first_parent[ind])))
                                 for ind in range(NUMBER_OF_NURSES)]
                    first_child, second_child = list(map(lambda elem: elem[0], combined)), list(map(lambda elem: elem[1], combined))
                    children.extend([first_child, second_child])
        return children
    def mutate(self, schedule: list[Nurse]) -> list[Nurse]:
        new_schedule = cp.deepcopy(schedule)
        # swap 2*n random nurses' 2 random days
        for _ in range(rnd.randint(1, 10)):
            nurse1, nurse2 = tuple(rnd.sample(list(range(NUMBER_OF_NURSES)), 2))
            day1, day2 = rnd.randint(0, NUMBER_OF_DAYS - 1), rnd.randint(0, NUMBER_OF_DAYS - 1)
            new_schedule[nurse1].schedule[day1], new_schedule[nurse2].schedule[day2] = new_schedule[nurse2].schedule[day2], new_schedule[nurse1].schedule[day1]
        return new_schedule
    def calculate_ranks(self, mutated_population: list[list[Nurse]]) -> list[list[int]]:
        size = range(len(mutated_population))
        dominance = [[0, i] for i in size]
        fitness = [self.evaluate_schedule(schedule) for schedule in mutated_population]
        for i in size:
            for j in size:
                if fitness[j] < fitness[i]:
                    dominance[i][0] += 1
        dominance.sort(key=lambda elem: elem[0])
        return dominance
    def select(self, mutated_population: list[list[Nurse]]) -> list[list[Nurse]]:
        ranks = self.calculate_ranks(mutated_population)
        return [mutated_population[ind] for ind in [ranks[i][1] for i in range(POPULATION_SIZE)]]
        # ranks.sort(key=lambda x: x[0])
        # return [ranks[i][1] for i in range(POPULATION_SIZE)]
    def get_sample(self) -> list[list[Nurse]]:
        # if the population size is smaller than the number of new entities then random.sample() will fail
        if len(self.population) < NUMBER_OF_NEW_ENTITIES:
            temp = cp.deepcopy(self.population)
            while len(temp) < NUMBER_OF_NEW_ENTITIES:
                temp.extend(temp)
            sample = rnd.sample(temp, NUMBER_OF_NEW_ENTITIES)
        else:
            sample = rnd.sample(cp.deepcopy(self.population), NUMBER_OF_NEW_ENTITIES)
        return sample
    def mutate_population(self) -> list[list[Nurse]]:
        return [self.mutate(schedule) if rnd.random() < MUTATION_RATE else schedule for schedule in cp.deepcopy(self.population)]
        # new_population = cp.deepcopy(self.population)
        # children = self.get_sample()
        # new_population.extend([self.mutate(schedule) for schedule in children])
        # return new_population
    def simulate(self) -> list[Nurse]:
        for _ in range(MAX_ITERATION_EA):
            children = self.cross_over()
            self.population.extend(children)
            mutated_population = self.mutate_population()
            self.population = self.select(mutated_population)
        return self.population[0]
    def experiment(self, results: list[int], times: list[float], information: str) -> None:
        average_penalty_point = sum(results) / len(results)
        average_run_time = sum(times) / len(times)
        LOG.info("Average penalty for best schedule %s: %d}", information, average_penalty_point)
        LOG.info("Average running time: %.4f s", average_run_time)
    def test(self):
        LOG.info("Patameters: number of days = %d and number of nurses = %d", NUMBER_OF_DAYS, NUMBER_OF_NURSES)
        experiment_count = 30
        results = []
        times = []
        for _ in range(experiment_count):
            start_time = perf_counter()
            best_schedule = self.simulate()
            end_time = perf_counter()
            results.append(self.evaluate_schedule(best_schedule))
            times.append(end_time - start_time)
        self.experiment(results, times, f"with Genetic Algorithm with {POPULATION_SIZE} entity in population")
        self.display(best_schedule,
                            end_time - start_time,
                            f"best schedule(last) with {self.__class__.__name__}")
