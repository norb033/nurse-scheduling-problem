"""
    A general interface for initialising both optimization algorithms.
"""

import copy as cp
import functools as fnc
import matplotlib.pyplot as plt
import numpy as np

from Nurse import Nurse
from constants import NUMBER_OF_DAYS, PENALTY, DISPLAY, RESULTS_FILE, WRITE_TO_OUT_FILE, DISPLAY_SCHEDULE_IN_CONSOLE
from logger import LOG

def _blend_colors(color_list: list[tuple]):
    new_color = color_list[0]
    print(color_list)
    for i in range(1, len(color_list)):
        alpha = 255 - ((255 - new_color[3]) * (255 - color_list[i][3]) / 255)
        red = (new_color[0] * (255 - new_color[3]) + color_list[i][0] * color_list[i][3]) / 255
        green = (new_color[1] * (255 - new_color[3]) + color_list[i][1] * color_list[i][3]) / 255
        blue = (new_color[2] * (255 - new_color[3]) + color_list[i][2] * color_list[i][3]) / 255
        new_color = ((red), (green), (blue), (alpha))
    return (new_color[0] / 255, new_color[1] / 255, new_color[2] / 255, new_color[3] / 255)

class Optimization:
    def __init__(self, initial_schedule: list[Nurse], minimum_staff_number: list[tuple]) -> None:
        self.initial_schedule = cp.deepcopy(initial_schedule)
        self.minimum_staff_number = cp.deepcopy(minimum_staff_number)
    def mutate(self, schedule: list[Nurse]) -> list[Nurse]:
        pass
    def evaluate_schedule(self, schedule: list[Nurse]) -> int:
        """Evaluates a schedule and gives a penalty score for each broken constraint"""
        overall_penalty = 0
        # Each schedule must satisfy the hospital's minimum coverage constraint
        # print(minimum_staff_number)
        for day in range(NUMBER_OF_DAYS):
            per_shift = [sum([nurse.schedule[day][0] for nurse in schedule]),
                        sum([nurse.schedule[day][0] for nurse in schedule]),
                        sum([nurse.schedule[day][0] for nurse in schedule])]
            multiplier = fnc.reduce(lambda count, elem: count + (1 if elem[1] < self.minimum_staff_number[day][elem[0]] else 0), enumerate(per_shift), 0)
            # print(per_shift)
            # print(multiplier)
            overall_penalty += multiplier * PENALTY.not_enough_nurse_on_shift

        # No nurse may work a night shift followed by a morning shift
        # for nurse in schedule:
        #     multiplier = sum([(1 if (nurse.schedule[day - 1][2] == 1 and nurse.schedule[day][0] == 1) else 0) for day in range(1, number_of_days)])
        multipliers = list(
                            map(
                                lambda nurse: sum([(1 if (nurse.schedule[day - 1][2] == 1 and nurse.schedule[day][0] == 1) else 0)
                                for day in range(1, NUMBER_OF_DAYS)]),
                                schedule))
        overall_penalty += sum(multipliers) * PENALTY.too_short_break_between_shifts

        # No nurse can work more than 1 shift a day
        # NOTE:
        #   In current state of the project this will not occur because it was by-passed with initialization
        #   and mutation will not mess up

        multipliers = list(map(lambda nurse: sum([1 if sum(nurse.schedule[day]) > 1 else 0 for day in range(NUMBER_OF_DAYS)]), schedule))
        overall_penalty += sum(multipliers) * PENALTY.too_many_shifts_a_day

        # Each nurse may work no more than 7 consecutive days / days in a row
        # schedule[0].schedule = [[0, 0, 0] for _ in range(number_of_days)]
        work_days = list(map(lambda nurse: [1 for day in range(NUMBER_OF_DAYS) if (sum(nurse.schedule[day]) > 0)], schedule))
        multipliers = list(map(lambda days: 1 if len(days) >= 7 else 0, work_days))
        overall_penalty += sum(multipliers) * PENALTY.too_many_work_days
        # print(sum(multipliers))
        # print(work_days)

        # print(multipliers)
        # for nurse in schedule:
        #     print(nurse.schedule)

        return overall_penalty
    def simulate(self) -> list[Nurse]:
        pass    
    def test(self) -> None:
        pass
    def display(self, schedule: list[Nurse], elapsed_time: float, information: str = "schedule") -> None:
        correlation_value = self.evaluate_schedule(self.initial_schedule)
        headers = "Day: \t" + "".join([f"\t  {i + 1}" for i in range(NUMBER_OF_DAYS)])
        if DISPLAY_SCHEDULE_IN_CONSOLE:
            print(headers)
            print("".join(["-"] * (NUMBER_OF_DAYS * 8 + 13)))

        if WRITE_TO_OUT_FILE:
            out_file = open(RESULTS_FILE, "a")
            out_file.write(f"{headers}\n{''.join(['-'] * (NUMBER_OF_DAYS * 8 + 13))}\n")

        for ind, nurse in enumerate(schedule):
            msg = f"Nurse {ind + 1}|" + "".join([f"\t{nurse.schedule[d][0]}:{nurse.schedule[d][1]}:{nurse.schedule[d][2]}" for d in range(NUMBER_OF_DAYS)])
            if DISPLAY_SCHEDULE_IN_CONSOLE:
                print(msg)
            if WRITE_TO_OUT_FILE:
                out_file.write(f"{msg}\n")

        current_schedule_value = self.evaluate_schedule(schedule)
        LOG.info("Penalty for %s: %d", information, current_schedule_value)
        if information != "initial schedule":
            LOG.info("Elapsed time: %.4f s", elapsed_time)
            LOG.info("Improvement: %.2f%s\n", (1 - current_schedule_value / correlation_value) * 100, '%')
        else:
            LOG.info("Running simulations..\n")

        if DISPLAY:
            image = np.zeros(NUMBER_OF_DAYS * 3 * 4)

            image = image.reshape((3, NUMBER_OF_DAYS, 4))
            for i in range(3):
                for j in range(NUMBER_OF_DAYS):
                    # find first nurse that works at day j in shift i
                    color = [0, 0, 0, 1]
                    color_list = []
                    for ind, nurse in enumerate(schedule):
                        if nurse.schedule[j][i] == 1:
                            # red, green, blue, _ = nurse.color
                            # color = red
                            # color = (color << 8) + green
                            # color = (color << 8) + blue
                            color_list.append(nurse.color)
                    if len(color_list) > 0:
                        color = _blend_colors(color_list)
                    image[i][j][0] = color[0]
                    image[i][j][1] = color[1]
                    image[i][j][2] = color[2]
                    image[i][j][3] = color[3]
                    # image[i][j] = 1 if i % 2 == 0 else 2 if j % 2 == 0 else 3
            print(image)

            row_labels = ["Morning shift", "Afternoon shift", "Night shift"]
            col_labels = [i + 1 for i in range(NUMBER_OF_DAYS)]
            plt.imshow(image) #cmap=ListedColormap([list(nurse.color) for nurse in schedule])
            plt.xticks(range(NUMBER_OF_DAYS), col_labels)
            plt.yticks(range(3), row_labels)
            plt.show()
    def experiment(self) -> None:
        pass
