"""This module collects all utility functions used in the project."""

import random as rnd

from Nurse import Nurse
from constants import NUMBER_OF_DAYS, NUMBER_OF_NURSES

def generate_colors(n):
    """ https://www.quora.com/How-do-I-generate-n-visually-distinct-RGB-colours-in-Python"""
    color_list = []
    red = int(rnd.random() * 256)
    green = int(rnd.random() * 256)
    blue = int(rnd.random() * 256)
    step = 256 / n
    for _ in range(n):
        red = (int(red + step)) % 256
        green = (int(green + step)) % 256
        blue = (int(blue + step)) % 256
        color_list.append((red, green, blue, rnd.randint(50, 255)))
    return color_list

def initalise_schedule():
    schedule = [Nurse(NUMBER_OF_DAYS) for index in range(NUMBER_OF_NURSES)]

    # generate colors for nurses
    colors = generate_colors(NUMBER_OF_NURSES)
    for (ind, nurse) in enumerate(schedule):
        nurse.set_color(colors[ind])

    for nurse in schedule:
        for day in range(NUMBER_OF_DAYS):
            shift = rnd.randint(0, 2)
            nurse.schedule[day][shift] = 1

    # check if this is a valid schedule - not necessary because each schedule will have a penalty
    return schedule
