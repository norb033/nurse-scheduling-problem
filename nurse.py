"""This module contains all the enities used for SA(Simulated Annealing) and GA(Genetic Algorithm)"""

class Nurse:
    """The Nurse class represents a nurse entity."""
    def __init__(self,number_of_days: int = 7):
        self.nr_consecutive_work_days = 0
        self.color = None
        # TODO: Introduce day off as 4 value in tuple
        self.schedule = [[0, 0, 0] for _ in range(number_of_days)] # for each day a tuple represents the shifts or day off
    def inc_work_days(self):
        self.nr_consecutive_work_days += 1
    def set_nr_work_days(self, nr:int = 0):
        self.nr_consecutive_work_days = nr
    def set_color(self, color: tuple):
        self.color = color
